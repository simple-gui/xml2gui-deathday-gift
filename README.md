# 🎄

## Deathday Gift

### Dedicated to Our Lady of La Salette :one:

Built with xml2gui Framework (C/C++, C#, Java/Kotlin, Python).  

Run with Python 3.6 and above (64 bit).

![Screenshot 1](https://gitlab.com/simple-gui/xml2gui-deathday-gift/-/raw/main/images/sh1.png)
